FROM openjdk:8
COPY ./target/RetailStoreDiscounts.jar RetailStoreDiscount.jar
ENTRYPOINT ["java","-jar","/RetailStoreDiscount.jar"]

